////
.. title: Unit 193 on the internet
.. type: text
.. hidetitle: True
////

== Unit 193's small corner of the internet ==

Howdy and welcome to my corner.  I'm not much into web design, but I help out with a few open source projects so you may find an interesting area on this server.

I maintain a few packages in Debian, am part of a few teams there, and am a Xubuntu developer.

== Some projects here ==
  * The Vanir repository, a small collection of new and updated packages.  Contact Unit 193 for more information.
  * Xubuntu Core ISOs, built off the xubuntu-core task which contains few default applications, are available in the link:/xubuntu/[Xubuntu] section.
  * link:/icebox/[Icebox], a custom version of Ubuntu using the Openbox window manager. *Note: This project was discontinued in 2017*
  * Maybe you thought I actually had something interesting on here, I don't.

== Contact ==
  * Unit193 on irc.libera.chat
  * Unit193 on irc.oftc.net
